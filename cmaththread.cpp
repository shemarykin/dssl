#include "cmaththread.h"
#include "math.h"
//------------------------------------------------------------------------------
/**
 * @brief   Конструктор.
 * @details Инициализируем переменные.
 * @param   balls   Коллекция шаров.
 * @param   mutex   Мьютекс.
 * @param   terminate   Флаг остановки потока.
 */
CMathThread::CMathThread(vecPBall& balls
                         , std::mutex& mutex
                         , bool& terminate)
    : m_balls(balls)
    , m_mutex(mutex)
    , m_terminate(terminate)
{
    m_thread = std::make_shared<std::thread>(std::bind(&CMathThread::execute, this));
    g_toLog("OK \t CMathThread::CMathThread()");
}
//------------------------------------------------------------------------------
/**
 * @brief   Деструктор.
 * @details Освобождаем ресурсы.
 */
CMathThread::~CMathThread()
{
    g_toLog("OK \t CMathThread::~CMathThread()");
}
//------------------------------------------------------------------------------
/**
 * @brief   Отсоединить поток.
 */
void CMathThread::detach()
{
    m_thread.get()->detach();
}
//------------------------------------------------------------------------------
/**
 * @brief   Рассчитать координаты шаров.
 * @details Пройтись по всем шарам из коллекции и для каждого вычислить новые
 * координаты.
 */
void CMathThread::computeXY()
{
    std::vector<pBall>::const_iterator it1, it2,
            begin = m_balls.begin(),
            end = m_balls.end();

    // расчитываем приращения координат для каждого шара
    for (it1 = begin; it1 != end; it1++)
    {
        // текущий шар
        CBall* currBall = it1->get();
        CXY currXY = currBall->getXY(), currDXY;

        // шары, по отношению к которым нужно посчитать сумму сил
        for (it2 = begin; it2 != end; it2++)
        {
            // шары должны быть разными
            if (it2 == it1) continue;

            CBall* partnerBall = it2->get();
            CXY partnerXY = partnerBall->getXY();

            #ifdef QT_DEBUG
            std::cout << "computeXY()   -currXY: " << currXY.x << ", " << currXY.y <<
                         "   -partnerXY: " << partnerXY.x << ", " << partnerXY.y << std::endl;
            #endif
            // считаем силу притяжения между шариками
            double r = 0.0, f = 0.0;
            r = hypot(currXY.x - partnerXY.x, currXY.y - partnerXY.y);

            /*
             * Если расстояние между центрами шаров меньше или равно 2х радиусов,
             * то расталкиваем шары.
             * Если, больше, то вычисляем силу притяжения и добавляем смещение.
             * */
            double r_2R = 2 * BALL_RADIUS, r_min = 0.01;

            if (r < r_min)
                r = r_min;

            // сила притяжения, вектор
            f = 1/r - 1/(r*r);

            if (r < r_2R) {
                f = -f;
            } else
            if (r == r_2R) {
                f = 0.0;
            }

            currDXY.x += f * (partnerXY.x - currXY.x) / r; // F * cos a
            currDXY.y += f * (partnerXY.y - currXY.y) / r; // F * sin a

            #ifdef QT_DEBUG
            std::cout << "computeXY()" << std::endl;
            std::cout << "\t currXY: " << currXY.x << ", " << currXY.y << std::endl;
            std::cout << "\t partnerXY: " << partnerXY.x << ", " << partnerXY.y << std::endl;
            std::cout << "\t currDXY: " << currDXY.x << ", " << currDXY.y << std::endl;
            #endif
        }
        // записываем приращение координат
        currBall->setDXY(currDXY);
    }

    // устанавливаем новые координаты шаров
    for (it1 = begin; it1 != end; ++it1)
    {
        CBall* b = it1->get();
        CXY xy = b->getXY(), dxy = b->getDXY();
        int k = 1; // для ускорения процесса
        xy.x += dxy.x * k;
        xy.y += dxy.y * k;
        b->setXY(xy);

        #ifdef QT_DEBUG
        std::cout << "Вычисляем:  " << xy.x << ", " << xy.y << std::endl;
        #endif
    }
}
//------------------------------------------------------------------------------
/**
 * @brief   Основная функция потока.
 * @details Постоянно вычисляем новые координаты шаров.
 */
void CMathThread::execute()
{
    while (!m_terminate)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        computeXY();
        lock.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_DELAY_MS));
    }
}
//------------------------------------------------------------------------------
