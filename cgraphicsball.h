#ifndef CGRAPHICSBALL_H
#define CGRAPHICSBALL_H
//------------------------------------------------------------------------------
#include "types.h"
#include <QObject>
#include <QGraphicsEllipseItem>
#include <QtGui>
//------------------------------------------------------------------------------
/**
 * @brief   Класс: графическое представление шара.
 */
class CGraphicsBall : public QObject, QGraphicsEllipseItem
{
    Q_OBJECT
    Q_PROPERTY(QRectF rect READ rect WRITE setRect)
public:
    CGraphicsBall(const CXY& xy, QObject *parent = 0);  // конструктор
    ~CGraphicsBall();                                   // деструктор

    void    setXY(const CXY& xy);       // переместить шар в заданную точку
    CXY     getXY() const;              // получить текущие координаты
    void    setLinkedBall(CBall* ball); // связать с реальным шаром
    CBall*  getLinkedBall() const;      // получить ссылку на реальный шар

private:
    CXY             m_xy;           // текущие координаты
    CBall*          m_ball;         // связанный реальный шар
};
//------------------------------------------------------------------------------
#endif // CGRAPHICSBALL_H
