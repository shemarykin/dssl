#-------------------------------------------------
#
# Project created by QtCreator 2016-03-30T09:06:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DSSL_qt
TEMPLATE = app


SOURCES += main.cpp\
    types.cpp \
    cmaththread.cpp \
    cgraphicsball.cpp \
    cgraphicsview.cpp

HEADERS  += \
    types.h \
    cmaththread.h \
    cgraphicsball.h \
    cgraphicsview.h

FORMS    +=
