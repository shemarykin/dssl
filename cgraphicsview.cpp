#include "cgraphicsview.h"
//------------------------------------------------------------------------------
/**
 * @brief   Конструктор.
 * @details Создание и инициализация сцены и инструментов отображения.
 * @param   parent  Родитель.
 */
CGraphicsView::CGraphicsView(QWidget *parent)
    : QGraphicsView(parent)
{
    // настраиваем таймер вывода изображений на экран
    m_timer = std::make_shared<QTimer>(new QTimer());
    m_timer.get()->setInterval(SCENE_UPDATE_INTERVAL_MS);
    m_timer.get()->setSingleShot(false);
    QObject::connect(m_timer.get(), SIGNAL(timeout()), this, SLOT(redrawScene()));
    m_timer.get()->start();

    // настраиваем сцену
    m_scene = std::make_shared<QGraphicsScene>(new QGraphicsScene);
    m_scene.get()->setSceneRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    this->setScene(m_scene.get());
    this->setRenderHint(QPainter::Antialiasing);
    this->setMouseTracking(true);

    m_mouseIsPressed = false;
    m_mouseIsMoving = false;

    g_toLog("OK \t CGraphicsView::CGraphicsView");
}
//------------------------------------------------------------------------------
/**
 * @brief   Деструктор.
 * @details Очистка сцены.
 */
CGraphicsView::~CGraphicsView()
{
    m_scene.get()->clear();
    m_timer.get()->stop();

    g_toLog("OK \t CGraphicsView::~CGraphicsView");
}
//------------------------------------------------------------------------------
/**
 * @brief   Инициализировать класс внешними переменными.
 * @param   balls   Коллекция шаров.
 */
void CGraphicsView::initialize(vecPBall* balls, std::mutex* mutex, bool* quit)
{
    m_balls = balls;
    m_mutex = mutex;
    m_quit = quit;
}
//------------------------------------------------------------------------------
/**
 * @brief   Добавить несколько шаров на форму.
 * @param   nBalls  Количество шаров.
 */
void CGraphicsView::addSomeBalls(int nBalls)
{
    // генерируем несколько шаров
    for (int i=0; i<nBalls; i++)
    {
        CXY xy;
        xy.x = rand() % WINDOW_WIDTH;
        xy.y = rand() % WINDOW_HEIGHT;
        addBall(xy);
    }
}
//------------------------------------------------------------------------------
void CGraphicsView::closeEvent(QCloseEvent *event)
{
    *m_quit = true;
    event->accept();
    QGraphicsView::closeEvent(event);
}
//------------------------------------------------------------------------------
/**
 * @brief   Обработчик события: нажали кнопку мыши.
 * @param   event   Событие.
 */
void CGraphicsView::mousePressEvent(QMouseEvent *event)
{
    // определяем координаты курсора
    QPointF cursor = mapToScene(event->pos());
    m_lastCursorPos = cursor;

    CXY xy;
    xy.x = cursor.x();
    xy.y = cursor.y();
    
    switch (event->buttons()) {
    case Qt::LeftButton :
        m_mouseIsPressed = true;
        break;        
    case Qt::RightButton :  // добавляем новый шарик
        addBall(xy);
        break;
    default: ;
    } // switch

    QGraphicsView::mousePressEvent(event);
}
//------------------------------------------------------------------------------
/**
 * @brief   Обработчик события: отпустили кнопку мыши.
 * @param   event   Событие.
 */
void CGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    QPointF cursor = mapToScene(event->pos());
    CXY xy;
    xy.x = cursor.x();
    xy.y = cursor.y();

    if (m_mouseIsPressed)
    {
        // лопаем шарик
        if (m_lastCursorPos == cursor && !m_mouseIsMoving)
        {
            dropBall(xy);
        }
        /*
        // перемещаеи шарик
        else
        {
            CXY lastXY;
            lastXY.x = m_lastCursorPos.x();
            lastXY.y = m_lastCursorPos.y();
            moveBall(lastXY, xy);
        }
        */
    }
    m_mouseIsPressed = false;
    m_mouseIsMoving = false;

    QGraphicsView::mousePressEvent(event);
}
//------------------------------------------------------------------------------
/**
 * @brief   Обработчик события: перемещаем указатель мыши.
 * @param   event   Событие.
 */
void CGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    // перемещаем шар
    if (m_mouseIsPressed)
    {
        QPointF cursor = mapToScene(event->pos());
        CXY xy;
        xy.x = cursor.x();
        xy.y = cursor.y();
        // нужно, чтобы когда отпустим мышь, шар не удалялся
        if (m_lastCursorPos != cursor) {
            m_mouseIsMoving = true;
        }
        CXY lastXY;
        lastXY.x = m_lastCursorPos.x();
        lastXY.y = m_lastCursorPos.y();
        moveBall(lastXY, xy);
        m_lastCursorPos = cursor;
    }
    QGraphicsView::mousePressEvent(event);
}
//------------------------------------------------------------------------------
/**
 * @brief   Добавить новый шарик с координатами х, у.
 * @details Добавить шарик в коллекцию и нарисовать на экране.
 * @param   xy  Координаты шара.
 */
void CGraphicsView::addBall(CXY xy)
{
    std::lock_guard<std::mutex> lock(*m_mutex);
    // создаем шарик
    pBall pB(new CBall(xy));
    
    // добавляем его в коллекцию
    m_balls->push_back(pB);

    // рисуем на экране
    CGraphicsBall* gBall = new CGraphicsBall(xy, this);
    gBall->setLinkedBall(pB.get());
    m_scene.get()->addItem((QGraphicsEllipseItem *)gBall);
}
//------------------------------------------------------------------------------
/**
 * @brief   Лопнуть шарик с координатами x, y.
 * @details Удалить шарик из коллекции, стереть со сцены.
 * @param   xy  Координаты.
 */
void CGraphicsView::dropBall(CXY& xy)
{
    std::lock_guard<std::mutex> lock(*m_mutex);
    if (m_balls->empty()) return;

    QList<QGraphicsItem*>::iterator gbIt;
    // определяем шар по его координатам
    for (gbIt = m_scene.get()->items().begin();
         gbIt != m_scene.get()->items().end();
         gbIt++)
    {
        // удаляем шар из коллекции и со сцены
        CGraphicsBall* gBall = (CGraphicsBall *)(*gbIt);
        CBall* ball = gBall->getLinkedBall();
        CXY bxy = ball->getXY();
        if (xy.x >= bxy.x - BALL_RADIUS && xy.x <= bxy.x + BALL_RADIUS
                && xy.y >= bxy.y - BALL_RADIUS && xy.y <= bxy.y + BALL_RADIUS)
        {
            // ищем шар в коллекции и удаляем его
            CBall* findIt = ball;
            for (ivecPBall it = m_balls->begin(); it != m_balls->end(); it++)
            {
                CBall* b = it->get();
                if (b == findIt) {
                    m_balls->erase(it);
                    break;
                }
            }
            m_scene.get()->removeItem(*gbIt);
            g_toLog("Шар удален");
            return;  // удаляем по одному шару за раз
        }
    }
}
//------------------------------------------------------------------------------
/**
 * @brief   Подвинуть шар с координатами х,у в новые координаты.
 * @param   xy  Текущие координаты шара.
 * @param   toXY    Новые координаты, куда надо переместить шар.
 */
void CGraphicsView::moveBall(CXY &xy, CXY &toXY)
{
    std::lock_guard<std::mutex> lock(*m_mutex);
    if (m_balls->empty()) return;

    QList<QGraphicsItem*> gBalls = m_scene.get()->items();
    for (ivecPBall pbIt = m_balls->begin(); pbIt != m_balls->end(); pbIt++)
    {
        // определяем шар по его координатам
        CBall* b = pbIt->get();
        CXY bxy = b->getXY();
        if (xy.x >= bxy.x - BALL_RADIUS && xy.x <= bxy.x + BALL_RADIUS
                && xy.y >= bxy.y - BALL_RADIUS && xy.y <= bxy.y + BALL_RADIUS)
        {
            CXY dxy;
            b->setXY(toXY);
            b->setDXY(dxy);
            return;  // перемещаем по одному шару за раз
        }
    }
}
//------------------------------------------------------------------------------
/**
 * @brief   Перерисовать сцену.
 */
void CGraphicsView::redrawScene()
{
    std::lock_guard<std::mutex> lock(*m_mutex);
    if (m_balls->empty()) return;

    QList<QGraphicsItem*> gBalls = m_scene.get()->items();
    QList<QGraphicsItem*>::iterator gbIt;
    for (gbIt = gBalls.begin(); gbIt != gBalls.end(); gbIt++)
    {
        CGraphicsBall* gBall = (CGraphicsBall *)(*gbIt);
        CBall* ball = gBall->getLinkedBall();
        gBall->setXY(ball->getXY());
        #ifdef QT_DEBUG
        std::cout << "Рисуем:  " << "  " << ball->getXY().x << ", " << ball->getXY().y << std::endl;
        #endif
    }
}
//------------------------------------------------------------------------------
