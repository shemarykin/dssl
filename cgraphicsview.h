#ifndef CGRAPHICSVIEW_H
#define CGRAPHICSVIEW_H
//------------------------------------------------------------------------------
#include "types.h"
#include "cgraphicsball.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTimer>
#include <QtGui>
//------------------------------------------------------------------------------
class CGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit CGraphicsView(QWidget *parent = 0);    // конструктор
    ~CGraphicsView();                               // деструктор

    void    initialize(vecPBall* balls, std::mutex* mutex, bool* quit); // инициализировать класс
    void    addSomeBalls(int nBalls);   // добавить шары
    
protected:
    virtual void closeEvent(QCloseEvent *event);        // событие закрытия окна
    virtual void mousePressEvent(QMouseEvent *event);   // нажали мышь
    virtual void mouseReleaseEvent(QMouseEvent *event); // отпустили мышь
    virtual void mouseMoveEvent(QMouseEvent *event);    // двигаем мышь

private:
    std::shared_ptr<QGraphicsScene> m_scene;    // сцена для рисования
    std::shared_ptr<QTimer>         m_timer;    // таймер для отрисовки сцены
    
    vecPBall*           m_balls;    // коллекция шаров
    std::mutex*         m_mutex;    // мьютекс
    bool*               m_quit;     // флаг закрытия окна

    void    addBall(CXY xy);   // добавить новый шар с координатами x, y
    void    dropBall(CXY &xy);  // лопнуть шар с координатами x, y
    void    moveBall(CXY& xy, CXY& toXY);   // переместить шар с координатами x, y

    bool    m_mouseIsPressed;   // признак нажатой левой кнопки мыши
    QPointF m_lastCursorPos;    // позиция курсора, когда была только нажата левая кнопка мыши
    bool    m_mouseIsMoving;    // признак того, что мы перемещаем курсор при нажатой кнопке

private slots:
    void    redrawScene();      // перерисовать сцену
};
//------------------------------------------------------------------------------
#endif // CGRAPHICSVIEW_H
