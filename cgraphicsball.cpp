#include "cgraphicsball.h"
#include <QPropertyAnimation>
//------------------------------------------------------------------------------
/**
 * @brief   Конструктор.
 * @details Нарисовать кружок в центре указанных координат.
 * @param   xy  Начальные координаты.
 * @param   parent  Родитель.
 */
CGraphicsBall::CGraphicsBall(const CXY& xy, QObject *parent)
    : m_xy(xy)
    , QObject(parent)
    , QGraphicsEllipseItem(QRectF(QPointF(xy.x - BALL_RADIUS, xy.y - BALL_RADIUS),
                                  QPointF(xy.x + BALL_RADIUS, xy.y + BALL_RADIUS)))
{
    this->setFlags(QGraphicsItem::ItemIsMovable);
    #ifdef QT_DEBUG
    std::cout << "new CGraphicsBall: " << m_xy.x << ", " << m_xy.y << std::endl;
    #endif
    g_toLog("OK \t CGraphicsBall::CGraphicsBall");
}
//------------------------------------------------------------------------------
/**
 * @brief   Деструктор.
 */
CGraphicsBall::~CGraphicsBall()
{
    g_toLog("OK \t CGraphicsBall::~CGraphicsBall");
}
//------------------------------------------------------------------------------
/**
 * @brief   Переместить шар в указанную точку.
 * @param   coordinates Новые координаты шара.
 */
void CGraphicsBall::setXY(const CXY& xy)
{
    QRectF currentRect(QPointF(m_xy.x - BALL_RADIUS, m_xy.y - BALL_RADIUS),
                       QPointF(m_xy.x + BALL_RADIUS, m_xy.y + BALL_RADIUS));
    QRectF targetRect(QPointF(xy.x - BALL_RADIUS, xy.y - BALL_RADIUS),
                      QPointF(xy.x + BALL_RADIUS, xy.y + BALL_RADIUS));

    // перемещаем шар
    if (!(m_xy == xy)) {
    QPropertyAnimation* animation = new QPropertyAnimation(this, "rect");
    animation->setStartValue(currentRect);
    animation->setEndValue(targetRect);
    animation->setDuration(BALL_ANIMATION_TIME_MS);
    animation->setEasingCurve(QEasingCurve::Linear);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
    }

    #ifdef QT_DEBUG
    std::cout << "CGraphicsBall::setXY   -xy: " << xy.x << ", " << xy.y <<
                 "   -m_xy: " << m_xy.x << ", " << m_xy.y << std::endl;
    #endif
    // сохраняем новые координаты
    m_xy = xy;
}
//------------------------------------------------------------------------------
/**
 * @brief   Вернуть текущие координаты шара.
 * @return  Текущие координаты.
 */
CXY CGraphicsBall::getXY() const
{
    return m_xy;
}
//------------------------------------------------------------------------------
/**
 * @brief   Связать с реальным шаром.
 * @param   ball    Реальный шар.
 */
void CGraphicsBall::setLinkedBall(CBall* ball)
{
    m_ball = ball;
}
//------------------------------------------------------------------------------
/**
 * @brief   Вернуть связь с реальным шаром.
 * @return  Реальный шар.
 */
CBall* CGraphicsBall::getLinkedBall() const
{
    return m_ball;
}
//------------------------------------------------------------------------------
