#ifndef CMATHTHREAD_H
#define CMATHTHREAD_H
//------------------------------------------------------------------------------
#include "types.h"
#include <mutex>
#include <thread>
//------------------------------------------------------------------------------
/**
 * @brief   Вычислительный поток.
 * @details Ведет расчет актуального положения шариков.
 */
class CMathThread
{
public:
    CMathThread(vecPBall& balls, std::mutex& mutex,
                bool& terminate);   // конструктор
    ~CMathThread();     // деструктор
    void detach();      // отсоединить поток

private:
    const vecPBall&                 m_balls;    // набор шаров
    std::shared_ptr<std::thread>    m_thread;   // дескриптор потока
    std::mutex& m_mutex;        // мьютекс
    bool&       m_terminate;    // флаг остановки потока

    void computeXY();   // рассчитать новые координаты шаров
    void execute();     // главная функция потока // флаг остановки потока

    void operator = (const CMathThread&);   // защита от копирования
    CMathThread(const CMathThread&);        // защита от копирования
};
//------------------------------------------------------------------------------
#endif // CMATHTHREAD_H
